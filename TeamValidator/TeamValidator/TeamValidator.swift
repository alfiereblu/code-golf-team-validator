//
//  TeamValidator.swift
//  TeamValidator
//
//  MIT License
//
//  Copyright (c) 2020 Roland Schmitz
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

import Foundation

/**
 Function which validates an array of team compositions and returns an array with the names of unbalanced teams.
 A balanced team is a team which has at least one team member for each of the following social styles:
 "Analytical", "Driver", "Amiable", "Expressive".
 
 - parameter teamCompositions:
    Input is an Array of team compositions. Each team composition is an array of Strings and contains the name of team
    followed by the social styles of the team members.
 
 - returns: An array with the names of all unbalanced teams.
 
 There is no need to deal with input error situations in the implementation, instead
 you can assume that the input is always formally correct, that is
 
 - Each team composition contains at least one element with the team name
 - All other elements in a team composition are exactly on of
   "Analytical", "Driver", "Amiable" or "Expressive"
*/
/*  F is a flag
    x is the generical element in the array A
    A is the Array to compare
 
 
    Explanation:
 
    I created the A array which contains the four social styles.
    With the first for-in cycle I enter in the matrix called teamComposition and the T stands for every single
    array in the matrix.
    With the second for-in cycle I enter in the A array and the x variable stands for the single social style in A.
    With the first if statement I check if at least one element of A is not (!) contained in T (that is the single team array)
    and eventually set the F flag to true value. In the second if statement if F is true It means that T doesn't contain at least
    one of the social styles, so I append in the result array only the team name. Then I set the flag to false value to restart
    the cycles.
    
 
*/

func findUnbalancedTeams(_ teamCompositions: [[String]]) -> [String] {
    
    var result = [String]()
    var F=false
    let A=["Driver","Analytical","Amiable","Expressive"]
    for T in teamCompositions{
            for x in A {
                if !T.contains(x) {
                    F=true
                }
            }
        if F {
            result+=[T[0]]
            F = !F
        }
     }
    return result
}
